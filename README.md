# Attitude Control System GUI




The software acts as an interface between Raspberry Pi and the Gefran 800P controller. It is used to communicate with the 800P controller (slave) through Raspberry Pi (master). The Gefran controller is connecting to a thermal vacuum chamber system and performs tasks on the system based on the command received.

----------


## Features

- Implements MQTT protocol for wireless communication
- Stores register values (data) in an influxDB "database"
- Write into the register values of the controller
- Read the register values from the controller
- Commands the controller to trigger a fail-safe feature (automatic shut down of the system in case of extreme environmental conditions)



